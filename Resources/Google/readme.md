Título: Charlero
--------------------
Descripción breve:
Simple app to test Push, Socket, Messages
--------------------
Descripción tecnica:
Server part: nodejs, mongodb, firebase, socket.io.
Client side: framework7, javascript,nodejs, cordova
After install app wants you to register your name & phone on the server. And verify your registration via push received. If registered & verified - invites to see everyone who is online/offline and chat. 
You may change your chat name and avatar in settings page.
If offline - you receive push notification about new msg come.

Descripción completa:
Comparte con tus mejores amigos la mensajeria privada

Aplicación desarrollada como proyecto del curso "Desarrollo de aplicaciones con tecnologías web" en AEG Ikastetxea.

Desarrolladores:
Alexei Sindiukov
Jose David Conesa
