
var firebaseToken;
var deviceId;
var receptorId;
var receptorName;
var receptorAvatar;
var userData;
var funct;
var online_users_array;
var offline_users_array;
var chats;
var socket;
var downloadURL;
var newMessageControl=false;
var baseUrl="http://192.168.1.107:3001/";
var socketUrl = "ws://192.168.1.107:3002";


/*var baseUrl="https://desarrollocharlero.herokuapp.com/";
var socketUrl = "ws://desarrollocharlero.herokuapp.com/";
var socketUrl2 = "ws://desarrollocharlero.herokuapp.com/";*/


(function (global) {
    "use strict";
    function onDeviceReady () {
        //        navigator.splashscreen.show();
        window.FirebasePlugin.getToken(function(token) {
            // save this server-side and use it to push notifications to this device
            console.log("firebasetoken: "+token);
            firebaseToken = token;
        }, function(error) {
            console.error(error);
        });

        window.FirebasePlugin.onNotificationOpen(function(notification) {

            console.log(notification);
            console.log("userData active"+userData.active);

            if(userData.active == false){
                myApp.popup(".register");
                $$('#code').val(notification.body);
                var code_id=notification.body;
                console.log(code_id);

                $$('#verificar').on('click', function(e){
                    e.preventDefault();
                    console.log('pasamos el onclick de boton #verificar');
                    var url = baseUrl+"verify/"+code_id;
                    funct = "verificar";
                    ajax(url,'put',null,funct);

                });
            }

        }, function(error) {
            console.error(error);
        });

        document.addEventListener("resume", onResume, false);
        console.log("onDeviceReady");
        console.log("cordova device: "+device.uuid);
        deviceId = device.uuid;
        findDeviceId(deviceId);

    }

    function onResume () {

    }

    function onBackButton(){
        console.log("click onBackButton");
        myApp.confirm('¿Desea salir?',"Salir", function () {
            navigator.app.exitApp();
        });
    }

    function onOffline() {
        myApp.alert('No hay conexión a internet',"Error");
    }

    document.addEventListener("deviceready", onDeviceReady, false);
    document.addEventListener("backbutton", onBackButton, false);
    document.addEventListener("offline", onOffline, false);

})(window);


// Initialize your app
var myApp = new Framework7();
// Export selectors engine
var $$ = Dom7; 
console.log("Iniciando app");  
// Add view
var mainView = myApp.addView('#view-1', {
});
var secondView = myApp.addView('#view-2', {
}); 

// Conversation flag
var conversationStarted = false;

// Init Messages
var myMessages = myApp.messages('.messages', {
    autoLayout:true, scrollMessages:true
});  

var myMessagebar = myApp.messagebar('.messagebar', {
    maxHeight: 200
});  

$$('#view-1').on('tab:show', function () {
    console.log("tab 1 click");

});

$$('#view-2').on('tab:show', function () {
    console.log("tab 2 click");

});

//---------------------------------------------- SOCKET --------------------------------------------------------------------




function iniciarSocket(socket){

    //SOCKET EVENTS
    socket.on('connected', function (data) {
        console.log("--------------------socket conected-----------------");
        console.log(data);
        socket.emit('senddeviceid',deviceId);
    });

    socket.on('getOnlineUsers', function(data){
        console.log("--------------------getOnlineUsers-----------------");
        $$("#usernumber").text(data.length);
        localStorage.setItem("onlineusers", JSON.stringify(data));
        online_users_array = data;
        renderingOnlineUsers(data);
    });

    socket.on('getOfflineUsers', function(data){
        console.log("--------------------getOfflineUsers-----------------");
        localStorage.setItem("offlineusers", JSON.stringify(data));
        offline_users_array = data;
        renderingOfflineUsers(data);
    });

    socket.on('getChats',function(data){
        console.log("--------------------getChats------------------------");
        console.log(data);
        chats = data;
        localStorage.setItem("chats", JSON.stringify(data));
        renderingChats(data);
    });

    socket.on('newUserConnected', function (data) {
        console.log("--------------------newUserConnected-----------------");

        // Añadir usuario online
        console.log(data);
        online_users_array =JSON.parse(localStorage.getItem("onlineusers")); 
        console.log("online_users_array");
        console.log(online_users_array);
        console.log("añadir usuario a online_users_array");
        online_users_array.push(data);
        console.log(online_users_array);
        renderingOnlineUsers(online_users_array);
        localStorage.setItem("onlineusers", JSON.stringify(online_users_array));
        $$("#usernumber").text(online_users_array.length);

        // Quitar usuario offline
        offline_users_array =JSON.parse(localStorage.getItem("offlineusers")); 
        console.log(offline_users_array);

        for(var i=0; i<offline_users_array.length;i++){
            if(offline_users_array[i].deviceId === data.deviceId){
                offline_users_array.splice(i, 1);;
            }
        }
        console.log(offline_users_array);

        renderingOfflineUsers(offline_users_array);
        localStorage.setItem("offlineusers", JSON.stringify(offline_users_array));

    });

    socket.on('userDisconnect', function (data) {
        console.log("--------------------userDisconnect-----------------");

        // Añadir usuario offline
        console.log(data);
        offline_users_array =JSON.parse(localStorage.getItem("offlineusers")); 
        console.log("offline_users_array");
        console.log(offline_users_array);
        console.log("añadir usuario a offline_users_array");
        offline_users_array.push(data);
        console.log(offline_users_array);
        renderingOfflineUsers(offline_users_array);
        localStorage.setItem("offlineusers", JSON.stringify(offline_users_array));

        // Quitar usuario online
        online_users_array =JSON.parse(localStorage.getItem("onlineusers")); 
        console.log(online_users_array);

        for(var i=0; i<online_users_array.length;i++){
            if(online_users_array[i].deviceId === data.deviceId){
                online_users_array.splice(i, 1);
            }
        }
        console.log(offline_users_array);

        renderingOnlineUsers(online_users_array);
        localStorage.setItem("onlineusers", JSON.stringify(online_users_array));
        $$("#usernumber").text(online_users_array.length);
    });

    socket.on("newChat", function(data){
        console.log("---------------------New chat--------------------------------");
        console.log(data);

        // Añadir chat nuevo
        chats = JSON.parse(localStorage.getItem("chats"));
        console.log("chats");
        console.log(chats);
        chats.push(data);
        console.log(chats);
        renderingChats(chats);
        localStorage.setItem("chats", JSON.stringify(chats));
    });

    socket.on('newMessageFromServer', function(data){
        console.log("--------------------newMessageFromServer-----------------");
        console.log(data);
        console.log('messageText: '+data);

        renderingMessages(data);

        console.log(myApp.views[0]);
        if(myApp.getCurrentView() == myApp.views[0]){

            console.log("Tab 1 activado");
            navigator.vibrate(1000);
            myApp.addNotification(
                {
                    message: data.name+': '+data.text,
                    closeOnClick:true
                    //onClose: mainView.router.load('view-2')
                });
        }else{
            console.log("Tab 2 activado");
        }

    });

    //Events
    $$('#btnSend').on('click',function(e){
        e.preventDefault();
        console.log('click send button');
        var message = $$('#messageText').val(); 
        console.log('messageText: '+message);
        var chat_exist = false;

        $$.each(chats, function (index, chat) {
            console.log(chat.receptorId);
            console.log(receptorId);
            if (chat.receptorId == receptorId && message){
                chat_exist = true;
            }
        });

        console.log(userData);
        console.log("ReceptorId ="+receptorId);

        if (chat_exist && message){
            console.log("El chat esta creado");
            console.log("Enviando mensaje");
            socket.emit('chat_p2p',message,userData,receptorId);
        }else{
            console.log("Creamos el chat");
            socket.emit('chat',userData,receptorId,receptorAvatar,receptorName);
            console.log("Enviando mensaje");
            socket.emit('chat_p2p',message,userData,receptorId);
        }
    });

    // Recuperamos mensajes entre emisor y receptor

    $$('.popup-userchat').on('popup:opened', function () {
        console.log('popup-userchat Popup opened');
        console.log(userData);
        console.log(receptorId);
        socket.emit('Messages',userData,receptorId);

    });

    socket.on('getMessages', function(data){
        console.log("--------------------getMessages-----------------");
        console.log(data);

        $$(".messages").empty();
        for(var i = data.length-1; i>=0; i--){

            console.log(data[i]);
            renderingMessages(data[i]);
        }
    });
    return socket;
}

function sendDeleteChat(recepId,socket){
    console.log("-----------------------------sendDeleteChat------------------------");
    console.log(socket);
    
    
    //SOCKET EVENTS
    socket.emit('deleteChat',userData,recepId);

    socket.on('getChats2',function(data){
        console.log("--------------------getChats------------------------");
        console.log(data);
        chats = data;
        localStorage.setItem("chats", JSON.stringify(data));
        renderingChats(data);
    });
}

//--------------------------------------------------------- PINTAR USUARIOS, CHATS Y MENSAJES --------------------------------------

function renderingOnlineUsers(data){
    console.log(data);
    if(data===undefined){
        $$("#onlineusers").empty();
    }else{
        console.log("rendering online users");
        console.log("data: "+data);
        $$("#onlineusers").empty();

        var template = $$('#onusers').html();
        var compiledTemplate = Template7.compile(template);
        var context = {users:data};
        var html = compiledTemplate(context); 
        $$('#onlineusers').html(html);
    }
}

function renderingOfflineUsers(data){
    console.log(data);
    if(data===undefined){
        $$("#offlineusers").empty();
    }else{
        var newdata = data.filter((elem)=>elem.deviceId!=deviceId);
        console.log("rendering offline users");
        console.log("data: "+data);     
        $$("#offlineusers").empty();

        var template = $$('#offusers').html();
        var compiledTemplate = Template7.compile(template);
        var context = {users:newdata};
        var html = compiledTemplate(context); 
        $$('#offlineusers').html(html);
    }
}

function renderingChats(data){

    if(data===undefined){
        $$("#chats").empty();
    }else{
        console.log("rendering chats");
        console.log("data: "+data);     
        $$("#chats").empty();

        var template = $$('#ch').html();
        var compiledTemplate = Template7.compile(template);
        var context = {chats:data};
        var html = compiledTemplate(context); 
        $$('#chats').html(html);
    }
}

function renderingMessages(data){
    console.log("localizamos data");
    console.log(data);
    console.log('--entramos a plantilla de vista de messages--');

    var messageType;

    if(data.emisorId == deviceId){
        messageType='sent';
        //clean message value
        $$("#messageText").val("");
    }else{
        messageType='received';
    }

    console.log("Pintar 1 mensaje");
    myMessages.addMessage({
        // Message text
        text: data.text,
        // Random message type
        type: messageType,
        // Avatar and name:
        avatar: data.avatar,
        name: data.name,
        day: data.fecha,
        time: data.time
    });

    // Update conversation flag
    conversationStarted = true;

}

//------------------------------------------------------------ PANTALLA DE SETTINGS ----------------------------------------------

$$('.options').on('popup:open', function () {
    console.log(userData);
    $$('#input_name').val(userData.name);
    $$('#imageFile').attr('src', userData.avatar);
});
/* camera usage funcciones */
// set options para iniciar Camera
function setOptions(srcType) {
    var options = {
        // Some common settings are 20, 50, and 100
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        // In this app, dynamically set the picture source, Camera or photo gallery
        sourceType: srcType,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: Camera.MediaType.PICTURE,
        allowEdit: true,
        correctOrientation: true  //Corrects Android orientation quirks
    }
    return options;
}
// taking a picture
$$('#photo_local').on('click', function(){
    var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
    getPhoto(srcType);
})
$$('#photo_camera').on('click', function(){
    var srcType = Camera.PictureSourceType.CAMERA;
    getPhoto(srcType);
})

$$('#photo_save').on('click', function(e){
    e.preventDefault();
    console.log('entramos a guardar formurario de options');
    console.log(userData.deviceId);
    var data= {
        name:$$('#input_name').val(),
        avatar:downloadURL
    }
    var url = baseUrl+"user/"+userData.deviceId;
    funct = "photoSave";
    ajax(url,'put',data,funct);
});

function getPhoto(srcType) {
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string  
    console.log('entramos a Camera function');
    var storageRef = firebase.storage().ref();
    var options = setOptions(srcType);
    var imgId = Date.now();
    navigator.camera.getPicture(
        function cameraSuccess(imageUri) {
            var getFileBlob = function(url, cb) {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", url);
                xhr.responseType = "blob";
                xhr.addEventListener('load', function() {
                    cb(xhr.response); 
                });
                xhr.send();
            };

            var blobToFile = function(blob, name) {
                blob.lastModifiedDate = new Date();
                blob.name = name;
                return blob;
            };
            var getFileObject = function(filePathOrUrl, cb) {
                getFileBlob(filePathOrUrl, function(blob) {
                    cb(blobToFile(blob, imgId + '.jpg'));
                });
            };

            getFileObject(imageUri, function(fileObject) {
                console.log(fileObject);
                var uploadTask = storageRef.child('avatar/' + imgId + '.jpeg').put(fileObject);
                uploadTask.on('state_changed', function(snapshot) {
                    console.log(snapshot);
                }, function(error) {
                    console.log(error);
                }, function() {
                    downloadURL = uploadTask.snapshot.downloadURL;
                    userData.avatar=downloadURL;
                    console.log(downloadURL);
                    $$("#imageFile").attr("src", downloadURL);
                });
            });


        }, function cameraError(error) {
            console.debug("Unable to obtain picture: " + error, "app");

        }, options);
};

/* end of camera usage */

//------------------------------------------------- PANTALLA DE REGISTRO -------------------------------------------------

$$('#register').on('click', function (e) {
    e.preventDefault();
    console.log("click register button");

    var username = $$('#username').val();
    funct = "register";
    console.log("username: "+username);
    console.log("deviceid: "+deviceId);
    console.log("firebaseToken: "+firebaseToken);

    var data= {
        name:username,
        deviceId:deviceId,
        firebaseToken:firebaseToken
    }
    var url = baseUrl+"user";

    ajax(url,'post',data,funct);
});

//---------------------------------------------------  chats popup ---------------------------------------------


function openChat(link_value){
    console.log("click userchat");
    console.log($$(link_value).attr('data-id'));
    console.log(offline_users_array);

    $$.each(online_users_array, function (index, user) {
        console.log(user.deviceId);
        // Si el chat existe
        if(user.deviceId == $$(link_value).attr('data-id')){
            receptorId = user.deviceId;
            receptorName = user.name;
            receptorAvatar = user.avatar;
        }
    });

    $$.each(offline_users_array, function (index, user) {
        console.log(user.deviceId);
        // Si el chat existe
        if(user.deviceId == $$(link_value).attr('data-id')){
            receptorId = user.deviceId;
            receptorName = user.name;
            receptorAvatar = user.avatar;
        }
    });

    if (!receptorAvatar){
        console.log("entra");
        receptorAvatar="img/unknowuser.png";
    }

    console.log("receptorId ="+receptorId);
    console.log(receptorAvatar);
    console.log(receptorName);
    myApp.showTab('#view-2');
    myApp.popup(".popup-userchat");
    $$("#chat_name").text(receptorName);
    $$("#user_avatar").attr("src", receptorAvatar);
}


//------------------------------------------------------- BORRAR CHAT -----------------------------------------

function borrarChat(link_value){
    console.log("Borrar chat");
    receptorId = $$(link_value).attr('data-id');
    myApp.confirm('¿Estas seguro?',"Borrar", function () {
        console.log("Confirmacion de borrar chat");
        console.log(socket);
        sendDeleteChat(receptorId,socket);
    });
};


//------------------------------------------------------- BUSCAR Y ACTIVAR USUARIO -----------------------------------------

function findDeviceId(deviceId){

    console.log("findDeviceId");
    console.log(deviceId);

    var url = baseUrl+"user/"+deviceId;
    funct = "findDeviceId";
    console.log("ajaxurl: "+url);
    ajax(url,'get',null,funct);

}

function sendActivateCode(deviceId){

    console.log("function sendActivateCode");
    console.log(deviceId);

    var url = baseUrl+"deviceId/"+deviceId;
    funct = "sendActivateCode";
    console.log("ajaxurl: "+url);

    ajax(url,'get',null,funct);
}

function checkActiveSuccess(data){
    console.log("Consulta realizada");
    if(data){
        if(data.active === true){
            //Ususario registrado y activado
            console.log("Usuario registrado y activado");
            socket = io.connect(socketUrl); 
            console.log(socket);
            iniciarSocket(socket);
        }else{
            //Usuario registrado pero sin activar
            console.log("Usuario registrado pero sin activar");
            sendActivateCode(data.deviceId);
        }

    }else{
        myApp.loginScreen(".login-screen");
    }
}

//-------------------------------------------------------- FUNCION AJAX ----------------------------------------------------


function ajax(url,method,data,funct){

    $$.ajax({ 
        url: url, 
        method: method, 
        data: data, 
        dataType:'json',
        success: function(data) {

            if(funct=="findDeviceId"){
                console.log("--------------FINDDEVICEID REALIZADO-----------------");
                console.log(data[0]);
                userData=data[0];
                checkActiveSuccess(userData);
            }else if(funct=="sendActivateCode"){
                console.log("--------------SENDACTIVATECODE REALIZADO-----------------");
                console.log(data[0]);
            }else if(funct =="register"){
                userData = data;
                console.log("--------------REGISTER REALIZADO-----------------");
                window.plugins.toast.show('Usuario registrado', 'long', 'bottom');
                myApp.closeModal('.login-screen');
            }else if(funct == "photoSave"){
                console.log("--------------PHOTOSAVE REALIZADO-----------------");
                console.log(data);
                userData = data;
                //console.log(data);
                window.plugins.toast.show('Usuario actualizado', 'long', 'bottom');
                myApp.closeModal('.options');
            }else if(funct == "verificar"){
                console.log("--------------VERIFICAR REALIZADO-----------------");
                // comprobamos los datos q han devuelto despues de verificacion del codigo
                if (data=="200"){
                    console.log("Validacion. 200. El codigo es correcto");
                    myApp.closeModal(".register");

                    findDeviceId(deviceId);
                }
                else if (data=="400"){console.log("Validacion. 400. El codigo NO es correcto")}
                // window.plugins.toast.show('Usuario registrado', 'long', 'bottom');
                // mainView.router.loadPage({pageName: 'code_verification'});
            }

        }, //on success 
        error: function(xhr) {

            if(funct=="findDeviceId"){
                console.log("--------------FINDDEVICEID NO REALIZADO-----------------");
            }else if(funct=="sendActivateCode"){
                console.log("--------------SENDACTIVATECODE NO REALIZADO-----------------");
            }else if(funct =="register"){
                console.log("--------------REGISTER NO REALIZADO-----------------");
                console.log("error");
            }else if(funct == "photoSend"){
                console.log("--------------PHOTOSAVE NO REALIZADO-----------------");
                console.log(error);
            }else if(funct == "verificar"){
                console.log("--------------VERIFICAR NO REALIZADO-----------------");
                console.log("Codigo no enviado: "+code_id);
                console.log("error");
            }


        } //on error });
    });
}


